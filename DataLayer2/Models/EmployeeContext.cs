﻿using System;
using System.Collections.Generic;
using System.Text;
using DataLayer2.Models;
using Microsoft.EntityFrameworkCore;


namespace DataLayer2
{
    class EmployeeContext: DbContext
    {
        public EmployeeContext(DbContextOptions options)
         : base(options)
        {
        }
        public DbSet<Employee> Employees { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=(localdb)\ProjectsV13;Initial Catalog=StoreDB;");
        }
    }
}
